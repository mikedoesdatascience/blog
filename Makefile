POST ?= ""

new-post:
	@docker run -it --rm \
		-v $(shell pwd):$(shell pwd) \
		-w $(shell pwd) \
		-u $(shell id -u):$(shell id -g) \
		klakegg/hugo:ext \
			-s site \
			new posts/$(POST)/index.md
	@git checkout -b $(POST)

gitpod-server:
	@docker run -it --rm \
		-v $(shell pwd):$(shell pwd) \
		-w $(shell pwd) \
		-u $(shell id -u):$(shell id -g) \
		-p 1313:1313 \
		klakegg/hugo:ext \
			-s site \
			server -D \
			--baseUrl $(shell gp url 1313) \
			--liveReloadPort=443 \
			--appendPort=false \
			--bind=0.0.0.0